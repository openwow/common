﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenWoW.Database.Web.Migrations
{
    public partial class Add_Currencies : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "wow_currency_category",
                columns: table => new
                {
                    build_id = table.Column<int>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    expansion_id = table.Column<int>(nullable: false),
                    flags = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wow_currency_category", x => new { x.build_id, x.id });
                    table.ForeignKey(
                        name: "fk_wow_currency_category_build_data_build_id",
                        column: x => x.build_id,
                        principalTable: "build_data",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "wow_currency",
                columns: table => new
                {
                    build_id = table.Column<int>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    category_id = table.Column<int>(nullable: false),
                    faction_id = table.Column<int>(nullable: true),
                    flags = table.Column<int>(nullable: false),
                    icon_id = table.Column<int>(nullable: false),
                    max_per_week = table.Column<int>(nullable: false),
                    max_quantity = table.Column<int>(nullable: false),
                    quality = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wow_currency", x => new { x.build_id, x.id });
                    table.ForeignKey(
                        name: "fk_wow_currency_build_data_build_id",
                        column: x => x.build_id,
                        principalTable: "build_data",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_currency_wow_currency_category_category_id",
                        columns: x => new { x.build_id, x.category_id },
                        principalTable: "wow_currency_category",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_currency_wow_icon_icon_id",
                        columns: x => new { x.build_id, x.icon_id },
                        principalTable: "wow_icon",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_wow_currency_build_id_category_id",
                table: "wow_currency",
                columns: new[] { "build_id", "category_id" });

            migrationBuilder.CreateIndex(
                name: "IX_wow_currency_build_id_icon_id",
                table: "wow_currency",
                columns: new[] { "build_id", "icon_id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "wow_currency");

            migrationBuilder.DropTable(
                name: "wow_currency_category");
        }
    }
}
