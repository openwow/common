﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenWoW.Database.Web.Migrations
{
    public partial class Add_TableHashLookup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "table_hash_lookup",
                columns: table => new
                {
                    __table_hash = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_table_hash_lookup", x => x.__table_hash);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "table_hash_lookup");
        }
    }
}
