﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenWoW.Database.Web.Migrations
{
    public partial class Add_SkillLine : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "wow_skill_line",
                columns: table => new
                {
                    build_id = table.Column<int>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    category_id = table.Column<int>(nullable: false),
                    flags = table.Column<int>(nullable: false),
                    icon_id = table.Column<int>(nullable: false),
                    parent_id = table.Column<int>(nullable: true),
                    parent_tier = table.Column<int>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true),
                    horde_name = table.Column<string>(nullable: true),
                    neutral_name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wow_skill_line", x => new { x.build_id, x.id });
                    table.ForeignKey(
                        name: "fk_wow_skill_line_build_data_build_id",
                        column: x => x.build_id,
                        principalTable: "build_data",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_skill_line_wow_icon_build_id_icon_id",
                        columns: x => new { x.build_id, x.icon_id },
                        principalTable: "wow_icon",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_skill_line_wow_skill_line_build_id_parent_id",
                        columns: x => new { x.build_id, x.parent_id },
                        principalTable: "wow_skill_line",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_wow_skill_line_build_id_icon_id",
                table: "wow_skill_line",
                columns: new[] { "build_id", "icon_id" });

            migrationBuilder.CreateIndex(
                name: "IX_wow_skill_line_build_id_parent_id",
                table: "wow_skill_line",
                columns: new[] { "build_id", "parent_id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "wow_skill_line");
        }
    }
}
