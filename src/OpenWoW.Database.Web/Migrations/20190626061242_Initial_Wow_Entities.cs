﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenWoW.Database.Web.Migrations
{
    public partial class Initial_Wow_Entities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "wow_icon",
                columns: table => new
                {
                    build_id = table.Column<int>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    hash = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wow_icon", x => new { x.build_id, x.id });
                    table.ForeignKey(
                        name: "fk_wow_icon_build_data_build_id",
                        column: x => x.build_id,
                        principalTable: "build_data",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "wow_class",
                columns: table => new
                {
                    build_id = table.Column<int>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    flags = table.Column<int>(nullable: false),
                    icon_id = table.Column<int>(nullable: false),
                    starting_level = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wow_class", x => new { x.build_id, x.id });
                    table.ForeignKey(
                        name: "fk_wow_class_build_data_build_id",
                        column: x => x.build_id,
                        principalTable: "build_data",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_class_wow_icon_icon_id",
                        columns: x => new { x.build_id, x.icon_id },
                        principalTable: "wow_icon",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "wow_race",
                columns: table => new
                {
                    build_id = table.Column<int>(nullable: false),
                    id = table.Column<int>(nullable: false),
                    faction = table.Column<int>(nullable: false),
                    flags = table.Column<int>(nullable: false),
                    female_icon_id = table.Column<int>(nullable: false),
                    male_icon_id = table.Column<int>(nullable: false),
                    starting_level = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    name_female = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_wow_race", x => new { x.build_id, x.id });
                    table.ForeignKey(
                        name: "fk_wow_race_build_data_build_id",
                        column: x => x.build_id,
                        principalTable: "build_data",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_race_wow_icon_female_icon_id",
                        columns: x => new { x.build_id, x.female_icon_id },
                        principalTable: "wow_icon",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "fk_wow_race_wow_icon_male_icon_id",
                        columns: x => new { x.build_id, x.male_icon_id },
                        principalTable: "wow_icon",
                        principalColumns: new[] { "build_id", "id" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_wow_class_build_id_icon_id",
                table: "wow_class",
                columns: new[] { "build_id", "icon_id" });

            migrationBuilder.CreateIndex(
                name: "IX_wow_race_build_id_female_icon_id",
                table: "wow_race",
                columns: new[] { "build_id", "female_icon_id" });

            migrationBuilder.CreateIndex(
                name: "IX_wow_race_build_id_male_icon_id",
                table: "wow_race",
                columns: new[] { "build_id", "male_icon_id" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "wow_class");

            migrationBuilder.DropTable(
                name: "wow_race");

            migrationBuilder.DropTable(
                name: "wow_icon");
        }
    }
}
