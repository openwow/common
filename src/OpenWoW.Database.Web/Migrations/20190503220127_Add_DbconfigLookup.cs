﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenWoW.Database.Web.Migrations
{
    public partial class Add_DbconfigLookup : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "dbconfig_lookup",
                columns: table => new
                {
                    __table_hash = table.Column<int>(nullable: false),
                    __layout_hash = table.Column<int>(nullable: false),
                    id_column = table.Column<short>(nullable: false),
                    field_types = table.Column<byte[]>(nullable: true),
                    array_sizes = table.Column<byte[]>(nullable: true),
                    field_names = table.Column<string[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbconfig_lookup", x => new { x.__table_hash, x.__layout_hash });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "dbconfig_lookup");
        }
    }
}
