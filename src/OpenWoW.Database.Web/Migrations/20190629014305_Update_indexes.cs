﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace OpenWoW.Database.Web.Migrations
{
    public partial class Update_indexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_wow_class_wow_icon_icon_id",
                table: "wow_class");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_currency_wow_currency_category_category_id",
                table: "wow_currency");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_currency_wow_icon_icon_id",
                table: "wow_currency");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_race_wow_icon_female_icon_id",
                table: "wow_race");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_race_wow_icon_male_icon_id",
                table: "wow_race");

            migrationBuilder.AddForeignKey(
                name: "fk_wow_class_wow_icon_build_id_icon_id",
                table: "wow_class",
                columns: new[] { "build_id", "icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_currency_wow_currency_category_build_id_category_id",
                table: "wow_currency",
                columns: new[] { "build_id", "category_id" },
                principalTable: "wow_currency_category",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_currency_wow_icon_build_id_icon_id",
                table: "wow_currency",
                columns: new[] { "build_id", "icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_race_wow_icon_build_id_female_icon_id",
                table: "wow_race",
                columns: new[] { "build_id", "female_icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_race_wow_icon_build_id_male_icon_id",
                table: "wow_race",
                columns: new[] { "build_id", "male_icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_wow_class_wow_icon_build_id_icon_id",
                table: "wow_class");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_currency_wow_currency_category_build_id_category_id",
                table: "wow_currency");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_currency_wow_icon_build_id_icon_id",
                table: "wow_currency");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_race_wow_icon_build_id_female_icon_id",
                table: "wow_race");

            migrationBuilder.DropForeignKey(
                name: "fk_wow_race_wow_icon_build_id_male_icon_id",
                table: "wow_race");

            migrationBuilder.AddForeignKey(
                name: "fk_wow_class_wow_icon_icon_id",
                table: "wow_class",
                columns: new[] { "build_id", "icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_currency_wow_currency_category_category_id",
                table: "wow_currency",
                columns: new[] { "build_id", "category_id" },
                principalTable: "wow_currency_category",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_currency_wow_icon_icon_id",
                table: "wow_currency",
                columns: new[] { "build_id", "icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_race_wow_icon_female_icon_id",
                table: "wow_race",
                columns: new[] { "build_id", "female_icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "fk_wow_race_wow_icon_male_icon_id",
                table: "wow_race",
                columns: new[] { "build_id", "male_icon_id" },
                principalTable: "wow_icon",
                principalColumns: new[] { "build_id", "id" },
                onDelete: ReferentialAction.Cascade);
        }
    }
}
