﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using OpenWoW.Database.Web.Contexts;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Database.Web.Factories
{
    public class WebContextFactory : IDesignTimeDbContextFactory<WebDbContext>
    {
        public WebDbContext CreateDbContext(string[] args) => new WebDbContext("Host=localhost;Port=50002;Username=openwow_site;Password=topsecret");
    }
}
