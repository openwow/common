﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Database.Web.CompositeEntities
{
    public struct RawDataWithId
    {
        public int Id;
        public object[] Data;
    }
}
