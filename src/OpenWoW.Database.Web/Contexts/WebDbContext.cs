﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Npgsql;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Database.Web.Extensions;
using System.Threading.Tasks;

namespace OpenWoW.Database.Web.Contexts
{
    public class WebDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<BuildData> BuildData { get; set; }
        public DbSet<BuildHash> BuildHash { get; set; }
        public DbSet<DbconfigLookup> DbconfigLookup { get; set; }
        public DbSet<DiffTooltip> DiffTooltip { get; set; }
        public DbSet<RawData> RawData { get; set; }
        public DbSet<RawMetadata> RawMetadata { get; set; }
        public DbSet<SubSite> SubSite { get; set; }
        public DbSet<TableHashLookup> TableHashLookup { get; set; }

        public DbSet<WowClass> WowClass { get; set; }
        public DbSet<WowCurrency> WowCurrency { get; set; }
        public DbSet<WowCurrencyCategory> WowCurrencyCategory { get; set; }
        public DbSet<WowIcon> WowIcon { get; set; }
        public DbSet<WowRace> WowRace { get; set; }
        public DbSet<WowSkillLine> WowSkillLine { get; set; }

        private readonly string _connectionString;
        private readonly LoggerFactory _loggerFactory;

        public WebDbContext(string connectionString, LoggerFactory loggerFactory = null) : base()
        {
            _connectionString = connectionString;
            _loggerFactory = loggerFactory;
        }

        public WebDbContext(DbContextOptions<WebDbContext> options) : base(options)
        { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                if (_loggerFactory != null)
                {
                    optionsBuilder = optionsBuilder.UseLoggerFactory(_loggerFactory);
                }
                optionsBuilder.UseNpgsql(_connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.UseSnakeCase();

            // Primary keys
            builder.Entity<BuildHash>()
                .HasKey(bh => new { bh.BuildId, bh._TableHash });

            builder.Entity<DbconfigLookup>()
                .HasKey(dl => new { dl._TableHash, dl._LayoutHash });

            builder.Entity<DiffTooltip>()
                .HasKey(dt => new { dt.BuildId, dt.EntityType, dt.EntityId });

            builder.Entity<RawData>()
                .HasKey(rd => new { rd.RowHash });

            builder.Entity<RawMetadata>()
                .HasKey(rm => new { rm.Locale, rm.BuildId, rm._TableHash, rm.RowId, rm.RowHash, rm.FromCache });

            builder.Entity<TableHashLookup>()
                .HasKey(thl => new { thl._TableHash });

            // Wow* tables are all keyed on (BuildId, Id)
            builder.Entity<WowClass>()
                .HasKey(w => new { w.BuildId, w.Id });

            builder.Entity<WowCurrency>()
                .HasKey(w => new { w.BuildId, w.Id });

            builder.Entity<WowCurrencyCategory>()
                .HasKey(w => new { w.BuildId, w.Id });

            builder.Entity<WowIcon>()
                .HasKey(w => new { w.BuildId, w.Id });

            builder.Entity<WowRace>()
                .HasKey(w => new { w.BuildId, w.Id });

            builder.Entity<WowSkillLine>()
                .HasKey(w => new { w.BuildId, w.Id });

            // Unique indexes
            builder.Entity<BuildData>()
                .HasIndex(bd => new { bd.Branch, bd.Build }).IsUnique();

            // Manually specify SubSite->BuildData relationship to use ON CASCADE SET NULL, yuck
            builder.Entity<SubSite>()
                .HasOne(ss => ss.Build)
                .WithMany()
                .HasForeignKey(ss => ss.BuildId)
                .OnDelete(DeleteBehavior.SetNull);

            #region SubSite seeding
            builder.Entity<SubSite>().HasData(
                new SubSite { Id = 1, Branch = WowBranch.Live, Hostname = "openwow.$" },
                new SubSite { Id = 2, Branch = WowBranch.PTR, Hostname = "ptr.openwow.$" },
                new SubSite { Id = 3, Branch = WowBranch.Beta, Hostname = "beta.openwow.$" },
                new SubSite { Id = 4, Branch = WowBranch.Classic, Hostname = "classic.openwow.$" }
            );
            #endregion
        }

        public NpgsqlConnection GetConnection() => (NpgsqlConnection)Database.GetDbConnection();
    }
}
