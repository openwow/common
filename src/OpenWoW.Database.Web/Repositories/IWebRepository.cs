﻿using OpenWoW.Common;
using OpenWoW.Database.Web.CompositeEntities;
using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OpenWoW.Database.Web.Repositories
{
    public interface IWebRepository
    {
        Task Initialize();
        Task<SubSite> GetSubSiteByBranch(WowBranch branch);
        Task<IEnumerable<SubSite>> GetSubSites();
        Task<DiffTooltip> GetDiffTooltip(int buildId, EntityType entityType, int entityId);

        Task<List<BuildData>> GetAllBuildData();
        Task<List<BuildData>> GetAllBuildDataByBranch(WowBranch branch);
        Task<BuildData> GetBuildDataById(int id);
        Task<BuildData> GetLatestBuildByBranch(WowBranch branch);

        Task<DbconfigLookup> GetDbconfigLookupByBuildIdAndTableHash(int buildId, int tableHash);
        Task<DbconfigLookup> GetDbconfigLookupByBuildIdAndTableHash(int buildId, uint tableHash);

        Task<RawData> GetRawDataByRowHash(Guid rowHash);
        Task<List<RawDataWithId>> GetRawDataByLocaleAndBuildIdAndTableHash(WowLocale locale, int buildId, int tableHash);

        Task<RawMetadata> GetRawMetadataByLocaleAndBuildIdAndTableNameAndRowId(WowLocale locale, int buildId, string tableName, int id);

        Task<TableHashLookup> GetTableHashLookupByName(string name);
        Task<TableHashLookup> GetTableHashLookupByTableHash(int tableHash);
        Task<TableHashLookup> GetTableHashLookupByTableHash(uint tableHash);

        Task<List<WowClass>> GetAllWowClassByBuildId(int buildId);
        Task<List<WowCurrencyCategory>> GetAllWowCurrencyCategoryByBuildId(int buildId);
        Task<List<WowRace>> GetAllWowRaceByBuildId(int buildId);
        Task<List<WowSkillLine>> GetAllWowSkillLineByBuildId(int buildId);
    }
}
