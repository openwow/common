﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;
using OpenWoW.Common;
using OpenWoW.Database.Web.CompositeEntities;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Z.EntityFramework.Plus;

namespace OpenWoW.Database.Web.Repositories
{
    public class WebRepository : IWebRepository
    {
        protected readonly IDistributedCache _cache;
        protected readonly WebDbContext _context;

        public WebRepository(IDistributedCache cache, WebDbContext context)
        {
            _context = context;
            _cache = cache;
        }

        public virtual async Task Initialize()
        {
            //_logger.LogInformation("Making sure database migrations are up to date");
            await _context.Database.MigrateAsync();
        }

        public async Task<SubSite> GetSubSiteByBranch(WowBranch branch)
        {
            return await _context.SubSite
                .FirstOrDefaultAsync(ss => ss.Branch == branch);
        }

        public virtual async Task<IEnumerable<SubSite>> GetSubSites()
        {
            return await _context.SubSite
                .OrderBy(x => x.Id)
                .ToListAsync();
        }

        public async Task<DiffTooltip> GetDiffTooltip(int buildId, EntityType entityType, int entityId)
        {
            return await _context.DiffTooltip
                .Where(dt => dt.BuildId == buildId && dt.EntityType == entityType && dt.EntityId == entityId)
                .Include(dt => dt.Build)
                .FirstOrDefaultAsync();
        }

        public async Task<List<BuildData>> GetAllBuildData()
        {
            return await _context.BuildData
                .ToListAsync();
        }

        public async Task<List<BuildData>> GetAllBuildDataByBranch(WowBranch branch)
        {
            return await _context.BuildData
                .Where(bd => bd.Branch == branch).ToListAsync();
        }

        public async Task<BuildData> GetBuildDataById(int id)
        {
            return await _context.BuildData
                .SingleOrDefaultAsync(bd => bd.Id == id);
        }

        public async Task<BuildData> GetLatestBuildByBranch(WowBranch branch)
        {
            return await _context.BuildData
                .Where(bd => bd.Branch == branch && _context.RawMetadata.Any(rm => rm.BuildId == bd.Id))
                .OrderByDescending(bd => bd.Build)
                .FirstOrDefaultAsync();
        }

        public async Task<DbconfigLookup> GetDbconfigLookupByBuildIdAndTableHash(int buildId, int tableHash)
        {
            return await _context.BuildHash
                .Where(bh => bh.BuildId == buildId && bh._TableHash == tableHash)
                .Join(
                    _context.DbconfigLookup,
                    bh => new { bh._TableHash, bh._LayoutHash },
                    dl => new { dl._TableHash, dl._LayoutHash },
                    (bh, dl) => dl
                )
                .FirstOrDefaultAsync();
        }

        public async Task<DbconfigLookup> GetDbconfigLookupByBuildIdAndTableHash(int buildId, uint tableHash) => await GetDbconfigLookupByBuildIdAndTableHash(buildId, unchecked((int)tableHash));

        public async Task<RawData> GetRawDataByRowHash(Guid rowHash)
        {
            return await _context.RawData
                .FirstOrDefaultAsync(rd => rd.RowHash == rowHash);
        }

        public async Task<List<RawDataWithId>> GetRawDataByLocaleAndBuildIdAndTableHash(WowLocale locale, int buildId, int tableHash)
        {
            return await _context.RawMetadata
                .Where(rm => rm.Locale == locale && rm.BuildId == buildId && rm._TableHash == tableHash)
                .Join(
                    _context.RawData,
                    rm => rm.RowHash,
                    rd => rd.RowHash,
                    (rm, rd) => new RawDataWithId
                    {
                        Id = rm.RowId,
                        Data = JsonConvert.DeserializeObject<object[]>(rd.JsonData),
                    }
                )
                .ToListAsync();
            
        }

        public async Task<RawMetadata> GetRawMetadataByLocaleAndBuildIdAndTableNameAndRowId(WowLocale locale, int buildId, string tableName, int id)
        {
            var tableHashLookup = await GetTableHashLookupByName(tableName);
            if (tableHashLookup == null)
            {
                throw new KeyNotFoundException();
            }

            return await _context.RawMetadata
                .FirstOrDefaultAsync(rm =>
                    rm.Locale == locale &&
                    rm.BuildId == buildId &&
                    rm._TableHash == tableHashLookup._TableHash &&
                    rm.RowId == id);
        }

        public async Task<TableHashLookup> GetTableHashLookupByName(string name)
        {
            return await _context.TableHashLookup
                .SingleOrDefaultAsync(thl => EF.Functions.ILike(thl.Name, name));
        }

        public async Task<TableHashLookup> GetTableHashLookupByTableHash(int tableHash)
        {
            return await _context.TableHashLookup
                .SingleOrDefaultAsync(thl => thl._TableHash == tableHash);
        }

        public async Task<TableHashLookup> GetTableHashLookupByTableHash(uint tableHash) => await GetTableHashLookupByTableHash(unchecked((int)tableHash));

        #region Wow tables
        public async Task<List<WowClass>> GetAllWowClassByBuildId(int buildId)
        {
            return await _context.WowClass
                .Where(wc => wc.BuildId == buildId)
                .Include(wc => wc.Icon)
                .ToListAsync();
        }

        public async Task<List<WowCurrencyCategory>> GetAllWowCurrencyCategoryByBuildId(int buildId)
        {
            return await _context.WowCurrencyCategory
                .Where(wcc => wcc.BuildId == buildId)
                .OrderBy(wcc => wcc.ExpansionId)
                .ThenBy(wcc => wcc.Name)
                .Include(wcc => wcc.Currencies)
                .ThenInclude(wc => wc.Icon)
                .ToListAsync();
        }

        public async Task<List<WowRace>> GetAllWowRaceByBuildId(int buildId)
        {
            return await _context.WowRace
                .Where(wr => wr.BuildId == buildId)
                .Include(wr => wr.FemaleIcon)
                .Include(wr => wr.MaleIcon)
                .ToListAsync();
        }

        public async Task<List<WowSkillLine>> GetAllWowSkillLineByBuildId(int buildId)
        {
            return await _context.WowSkillLine
                .Where(wsl => wsl.BuildId == buildId)
                .Include(wsl => wsl.Icon)
                .ToListAsync();
        }
        #endregion
    }
}
