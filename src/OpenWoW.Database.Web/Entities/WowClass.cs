﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class WowClass
    {
        [JsonIgnore]
        public int BuildId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int Flags { get; set; }
        [JsonIgnore]
        public int IconId { get; set; }
        public int StartingLevel { get; set; }
        public string Name { get; set; }

        // References
        [ForeignKey("BuildId"), JsonIgnore]
        public BuildData Build { get; set; }

        [ForeignKey("BuildId, IconId")]
        public WowIcon Icon { get; set; }
    }
}
