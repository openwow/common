﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class WowCurrency
    {
        [JsonIgnore]
        public int BuildId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [JsonIgnore]
        public int CategoryId { get; set; }
        public int? FactionId { get; set; }
        public int Flags { get; set; }
        [JsonIgnore]
        public int IconId { get; set; }
        public int MaxPerWeek { get; set; }
        public int MaxQuantity { get; set; }
        public int Quality { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        // References
        [ForeignKey("BuildId")]
        [JsonIgnore]
        public BuildData Build { get; set; }

        [ForeignKey("BuildId, CategoryId")]
        public WowCurrencyCategory Category { get; set; }

        [ForeignKey("BuildId, IconId")]
        public WowIcon Icon { get; set; }
    }
}
