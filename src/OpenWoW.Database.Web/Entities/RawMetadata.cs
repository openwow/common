﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class RawMetadata
    {
        // Column ordering matters, largest->smallest
        public Guid RowHash { get; set; }
        public DateTime FirstSeen { get; set; }
        public DateTime LastSeen { get; set; }
        public int SeenCount { get; set; }
        public int BuildId { get; set; }
        public int RowId { get; set; }
        public int _TableHash { get; set; }
        public WowLocale Locale { get; set; }
        public bool Active { get; set; }
        public bool FromCache { get; set; }

        [ForeignKey("BuildId")]
        public BuildData Build { get; set; }

        [NotMapped]
        public uint TableHash
        {
            get
            {
                return unchecked((uint)_TableHash);
            }
            set
            {
                _TableHash = unchecked((int)value);
            }
        }
    }
}
