﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class TableHashLookup
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int _TableHash { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public uint TableHash
        {
            get
            {
                return unchecked((uint)_TableHash);
            }
            set
            {
                _TableHash = unchecked((int)value);
            }
        }
    }
}
