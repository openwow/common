﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class SubSite
    {
        public int Id { get; set; }
        public int? BuildId { get; set; }
        public WowBranch Branch { get; set; }
        public string Hostname { get; set; }

        public BuildData Build { get; set; }
    }
}
