﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class RawData
    {
        public Guid RowHash { get; set; }
        public string JsonData { get; set; }
    }
}
