﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class WowIcon
    {
        [JsonIgnore]
        public int BuildId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public string Name { get; set; }
        public string Hash { get; set; }

        // References
        [ForeignKey("BuildId"), JsonIgnore]
        public BuildData Build { get; set; }
    }
}
