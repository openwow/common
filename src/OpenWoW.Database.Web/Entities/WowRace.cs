﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class WowRace
    {
        [JsonIgnore]
        public int BuildId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int Faction { get; set; }
        public int Flags { get; set; }
        [JsonIgnore]
        public int FemaleIconId { get; set; }
        [JsonIgnore]
        public int MaleIconId { get; set; }
        public int StartingLevel { get; set; }
        public string Name { get; set; }
        public string NameFemale { get; set; }

        // References
        [ForeignKey("BuildId"), JsonIgnore]
        public BuildData Build { get; set; }

        [ForeignKey("BuildId, FemaleIconId")]
        public WowIcon FemaleIcon { get; set; }

        [ForeignKey("BuildId, MaleIconId")]
        public WowIcon MaleIcon { get; set; }
    }
}
