﻿using OpenWoW.Common;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenWoW.Database.Web.Entities
{
    public class BuildData
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        // Column ordering matters, largest->smallest. Version is varchar and will be packed efficiently.
        public DateTime Date { get; set; }
        public int Build { get; set; }
        public WowBranch Branch { get; set; }
        public Expansion Expansion { get; set; }

        [MaxLength(8)] // 10.10.10
        public string Version { get; set; }
    }
}
