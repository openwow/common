﻿using Newtonsoft.Json;
using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class WowCurrencyCategory
    {
        [JsonIgnore]
        public int BuildId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int ExpansionId { get; set; }
        public int Flags { get; set; }
        public string Name { get; set; }

        [NotMapped]
        public Expansion Expansion => (Expansion)ExpansionId;

        // References
        [ForeignKey("BuildId"), JsonIgnore]
        public BuildData Build { get; set; }

        public List<WowCurrency> Currencies { get; set; }
    }
}
