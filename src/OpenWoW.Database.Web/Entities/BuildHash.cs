﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class BuildHash
    {
        public int BuildId { get; set; }
        public int _TableHash { get; set; }
        public int _LayoutHash { get; set; }

        [ForeignKey("BuildId")]
        public BuildData Build { get; set; }

        [NotMapped]
        public uint TableHash
        {
            get
            {
                return unchecked((uint)_TableHash);
            }
            set
            {
                _TableHash = unchecked((int)value);
            }
        }

        [NotMapped]
        public uint LayoutHash
        {
            get
            {
                return unchecked((uint)_LayoutHash);
            }
            set
            {
                _LayoutHash = unchecked((int)value);
            }
        }
    }
}
