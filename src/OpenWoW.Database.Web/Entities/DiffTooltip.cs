﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class DiffTooltip
    {
        public int BuildId { get; set; }
        public int EntityId { get; set; }
        public EntityType EntityType { get; set; }
        public string Icon { get; set; }
        public string JsonData { get; set; }

        [ForeignKey("BuildId")]
        public BuildData Build { get; set; }
    }
}
