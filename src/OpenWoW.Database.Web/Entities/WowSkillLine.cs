﻿using Newtonsoft.Json;
using OpenWoW.Common.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace OpenWoW.Database.Web.Entities
{
    public class WowSkillLine
    {
        [JsonIgnore]
        public int BuildId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        public int CategoryId { get; set; }
        public int Flags { get; set; }
        [JsonIgnore]
        public int IconId { get; set; }
        public int? ParentId { get; set; }
        public int ParentTier { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string HordeName { get; set; }
        public string NeutralName { get; set; }

        [NotMapped]
        public SkillLineCategory Category => (SkillLineCategory)CategoryId;

        // References
        [ForeignKey("BuildId"), JsonIgnore]
        public BuildData Build { get; set; }

        [ForeignKey("BuildId, IconId")]
        public WowIcon Icon { get; set; }

        [ForeignKey("BuildId, ParentId"), JsonIgnore]
        public WowSkillLine Parent { get; set; }

        [JsonIgnore]
        public List<WowSkillLine> Children { get; set; }
    }
}
