﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Database.Web.Extensions
{
    public static class IServiceCollectionExtensions
    {
        public static void ConfigurePostgres(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<WebDbContext>(options =>
            {
                options.UseNpgsql(config.GetConnectionString("Database_Web"));
                options.EnableSensitiveDataLogging();
            });

            services.AddScoped<IWebRepository, WebRepository>();
        }
    }
}
