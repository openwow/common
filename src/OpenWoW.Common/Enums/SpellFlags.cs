﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenWoW.Common
{
    [Flags]
    public enum SpellFlags1 : uint
    {
        OnNextRanged = 0x00000002,
        OnNextSwing = 0x00000004,
        // IsReplenishment = 0x00000008, // Spell-Editor-GUI, ?? on more spells than just Replenishment
        Ability = 0x00000010,
        TradeskillRecipe = 0x00000020,
        Passive = 0x00000040,
        HiddenAura = 0x00000080,
        HiddenCastTime = 0x00000100,
        [Display(Description = "Targets main-hand item")]
        TargetsMainHandItem = 0x00000200,
        OnNextSwingMobs = 0x00000400,
        [Display(Description = "Can only be cast during day")]
        RequiresDayTime = 0x00001000,
        [Display(Description = "Can only be cast during night")]
        RequiresNightTime = 0x00002000,
        [Display(Description = "Can only be cast indoors")]
        RequiresIndoors = 0x00004000,
        [Display(Description = "Can only be cast outdoors")]
        RequiresOutdoors = 0x00008000,
        NoShapeshift = 0x00010000,
        [Display(Description = "Requires stealth")]
        RequiresStealth = 0x00020000,
        DoesNotChangeSheathState = 0x00040000,
        DamageBasedOnCasterLevel = 0x00080000,
        [Display(Description = "Stops auto attack")]
        StopAutoAttack = 0x00100000,
        [Display(Description = "Cannot be avoided")]
        Unavoidable = 0x00200000,
        TrackTargetWhileCasting = 0x00400000,
        [Display(Description = "Can be cast while dead")]
        AllowWhileDead = 0x00800000,
        [Display(Description = "Can be cast while mounted")]
        AllowWhileMounted = 0x01000000,
        DelayedRecoveryStartTime = 0x02000000,
        // Negative = 0x04000000, // Spell-Editor-GUI, ??
        AllowWhileSitting = 0x08000000,
        [Display(Description = "Can only be cast out of combat")]
        RequiresNoCombat = 0x10000000,
        [Display(Description = "Disregards immunity effects")]
        DisregardImmunity = 0x20000000,
        [Display(Description = "Breakable by damage")]
        BreakableByDamage = 0x40000000,
        [Display(Description = "Buff cannot be canceled")]
        UncancelableAura = 0x80000000,
    }

    [Flags]
    public enum SpellFlags2 : uint
    {
        RequiresNoPet = 0x00000001,
        UseAllPower = 0x00000002,
        Channeled = 0x00000004,
        CannotBeRedirected = 0x00000008,
        [Display(Description = "Can be cast while stealthed")]
        AllowWhileStealthed = 0x00000020,
        Channeled2 = 0x00000040, // Spell-Editor-GUI, not sure how this differs from 0x4
        [Display(Description = "Can't be reflected")]
        CannotBeReflected = 0x00000080,
        [Display(Description = "Requires target to be out of combat")]
        RequiresTargetNoCombat = 0x00000100,
        [Display(Description = "Starts auto-attack")]
        StartAutoAttack = 0x00000200,
        [Display(Description = "Generates no threat")]
        NoThreat = 0x00000400,
        Pickpocket = 0x00001000,
        [Display(Description = "Changes viewpoint")]
        ChangeViewpoint = 0x00002000, // Spell-Editor-GUI, seems to be all spells that move your viewpoint to somewhere/something else
        [Display(Description = "Tracks target while channeling")]
        TrackTargetWhileChanneling = 0x00004000,
        [Display(Description = "Dispels buffs on mechanic immunity")]
        BreakAurasIfImmune = 0x00008000,
        [Display(Description = "Disregards school immunities")]
        DisregardSchoolImmunity = 0x00010000,
        // ?? = 0x00020000, // Spell-Editor-GUI, "Unautoscalable by Pet" ??
        CrowdControl = 0x00040000, // Spell-Editor-GUI, "Stun Polymorph Daze Hex"
        [Display(Description = "Cannot target self")]
        CannotTargetSelf = 0x00080000,
        RequiresComboPoints = 0x00100000,
        RequiresComboPoints2 = 0x00400000,
        Fishing = 0x01000000,
        // ?? = 0x04000000, // Spell-Editor-GUI, "Focus Targeting Macro" ??
        // ?? = 0x10000000, // Spell-Editor-GUI, "Hidden in Aura Bar" ?? SpellFlags1.HiddenAura exists
        // ?? = 0x20000000, // Spell-Editor-GUI, "Channel Display Name" ??
        [Display(Description = "Activates when dodged")]
        ActivateWhenDodged = 0x40000000,
    }

    [Flags]
    public enum SpellFlags3 : uint
    {
        // ?? = 0x00000001, // Spell-Editor-GUI, "Can Target Dead Unit or Corpse"
        // ?? = 0x00000002, // Spell-Editor-GUI, "Vanish, Shadowform, Ghost" not a generic name for effect
        [Display(Description = "Doesn't require line of sight")]
        DoesNotRequireLOS = 0x00000004, // Spell-Editor-GUI
        Stance = 0x00000010,
        StartNextSpell = 0x00000020,
        [Display(Description = "Required untapped target")]
        RequiresUntappedTarget = 0x00000040,
        // ?? = 0x00000800, // Spell-Editor-GUI, "Health Funnel" not a generic name for effect
        [Display(Description = "Hits multiple targets (Melee)")]
        MeleeMultipleTargets = 0x00001000, // Spell-Editor-GUI, "Cleave, Heart Strike, Maul, Sunder Armor, Swipe"
        TargetMustBeOwnItem = 0x00002000,
        TameBeast = 0x00010000,
        [Display(Description = "Doesn't rest auto-attack timer")]
        DoesNotResetAutoAttack = 0x00020000,
        RequiresDeadPet = 0x00040000,
        ShapeshiftAllowedButNotRequired = 0x00080000,
        ReducesDamageTaken = 0x002000000,
        // = 0x008000000, // Spell-Editor-GUI, "Requires Fishing Pole" includes a bunch of weapon enchants, probably not that
        [Display(Description = "Cannot critically hit")]
        CannotCriticallyHit = 0x20000000,
        // = 0x40000000, // Spell-Editor-GUI, "Triggered can Trigger Proc" ??
        FoodBuff = 0x80000000,
    }

    [Flags]
    public enum SpellFlags4 : uint
    {
        Blockable = 0x00000008,
        Resurrection = 0x00000010, // Spell-Editor-GUI, "Ignore Resurrection Timer" doesn't seem accurate
        // ?? = 0x00000080, // Spell-Editor-GUI, "Stack for Different Casters" includes a lot of spells that don't stack that way
        // ?? = 0x00000100, // Spell-Editor-GUI, "Only Target Players" includes a lot of random spells
        // ?? = 0x00000200, // Spell-Editor-GUI, "Triggered can Trigger Proc 2" ??
        RequiresMainHand = 0x00000400,
        [Display(Description = "Can only be cast in battlegrounds")]
        RequiresBattleground = 0x00000800,
        [Display(Description = "Can only target ghosts")]
        OnlyTargetGhosts = 0x00001000,
        HideChannelBar = 0x00002000,
        CancelAuraOnAttack = 0x00004000,
        AutoShot = 0x00008000,
        // ?? = 0x00010000, // Spell-Editor-GUI, "Cannot Trigger Proc" no idea how to verify that
        [Display(Description = "Does not engage target")]
        DoNotEngageTarget = 0x00020000,
        [Display(Description = "Cannot miss")]
        CannotMiss = 0x00040000,
        // ?? = 0x00080000, // Spell-Editor-GUI, "Disable Procs" no idea how to verify that
        [Display(Description = "Persists through death")]
        PersistThroughDeath = 0x00100000,
        RequiresWand = 0x00400000,
        RequiresOffhand = 0x01000000,
        // ?? = 0x02000000, // Spell-Editor-GUI, "Can Proc with Triggered" ??
        // ?? = 0x04000000, // Spell-Editor-GUI, "Drain Soul" includes waaaay more spells
        InternalSpellDontLog = 0x10000000
    }

    [Flags]
    public enum SpellFlags5 : uint
    {
        // ?? = 0x00000001, // Spell-Editor-GUI, "Ignore All Resistances" no way to verify
        // ?? = 0x00000002, // Spell-Editor-GUI, "Proc Only on Caster" weird mix of 3 spells
        [Display(Description = "Buff keeps ticking while logged off")]
        AuraTicksWhileLoggedOff = 0x00000004,
        [Display(Description = "Cannot be stolen by spell-stealing spells")]
        CannotBeStolen = 0000000040,
        // ?? = 0x00000080, // Spell-Editor-GUI, "Triggered" ??
        // ?? = 0x00000100, // Spell-Editor-GUI, "Fixed Damage" ??
        // ?? = 0x00000200, // Spell-Editor-GUI, "Activate from Event" a few procs, execute effects?
        // ?? = 0x00000400, // Spell-Editor-GUI, "Spell vs Extended Cost" ??
        DamageDoesNotBreakAuras = 0x00004000,
        [Display(Description = "Cannot be cast in arena")]
        DisallowInArena = 0x00010000,
        [Display(Description = "Can be cast in arena")]
        AllowInArena = 0x00020000,
        // ?? = 0x00040000, // Spell-Editor-GUI, "Area Target Chain" ??
        // ?? = 0x01000000, // Spell-Editor-GUI, "Don't Check Selfcast Power" ??
        // ?? = 0x02000000, // Spell-Editor-GUI, "Pet Scaling" ??
        // ?? = 0x04000000, // Spell-Editor-GUI, "Can Only be Casted in Outland" includes Flying Broom, err
        // ?? = 0x10000000, // Spell-Editor-GUI, "Aimed Shot" more than Aimed SHot
        // ?? = 0x80000000, // Spell-Editor-GUI, "Polymorph" more than just polymorph effects
    }

    [Flags]
    public enum SpellFlags6 : uint
    {
        [Display(Description = "Uses no reagents during arena preparation")]
        NoReagentsInArenaPreparation = 0x00000002,
        [Display(Description = "Can be cast while stunned")]
        AllowWhileStunned = 0x00000008,
        OnlyOneAura = 0x00000020,
        StartAuraPeriodicTickAtApply = 0x00000200,
        // ?? = 0x00000400, // Spell-Editor-GUI, "Hide Duration" ??
        // ?? = 0x00000800, // Spell-Editor-GUI, "Allow Target of Target as Target" ??
        // ?? = 0x00001000, // Spell-Editor-GUI, "Cleave" ??
        // ?? = 0x00002000, // Spell-Editor-GUI, "Haste Affect Duration" ??
        // ?? = 0x00008000, // Spell-Editor-GUI, "Inflict on Multiple Targets" ??
        // ?? = 0x00010000, // Spell-Editor-GUI, "Speical Item Class Check" ??
        [Display(Description = "Can be cast while feared")]
        AllowWhileFeared = 0x00020000,
        [Display(Description = "Can be cast while confused")]
        AllowWhileConfused = 0x00040000,
        // ?? = 0x00080000, // Spell-Editor-GUI, "Don't Turn during Casting" ??
        // ?? = 0x08000000, // Spell-Editor-GUI, "Don't Show Aura if Self-Cast" ??
        // ?? = 0x10000000, // Spell-Editor-GUI, "Don't Show Aura if Not Self-Cast" ??
        UsePhysicalHit = 0x80000000,
    }

    [Flags]
    public enum SpellFlags7 : uint
    {
        // ?? = 0x00000001, // Spell-Editor-GUI, "Don't Display Cooldown" ?? doesn't seem right
        [Display(Description = "Can only be cast in arenas")]
        RequiresArena = 0x00000002,
        // ?? = 0x00000004, // Spell-Editor-GUI, "Ignore Caster Auras" ??
        // ?? = 0x00000008, // Spell-Editor-GUI, "Assist Ignore Immune Flag" ??
        // ?? = 0x00000040, // Spell-Editor-GUI, "Spell Cast Event" ??
        WillNotHitCrowdControlledTargets = 0x00000100,
        // ?? = 0x00000400, // Spell-Editor-GUI, "Can Target Possessed Friends" ??
        [Display(Description = "Cannot be cast in raids")]
        DisallowInRaids = 0x00000800,
        // ?? = 0x00001000, // Spell-Editor-GUI, "Castable while on Vehicle" ?? not sure how to verify
        CanTargetInvisible = 0x00002000,
        // ?? = 0x00020000, // Spell-Editor-GUI, "Mount" ?? not just mount spells
        // ?? = 0x00040000, // Spell-Editor-GUI, "Cast by Charmer" ??
        // ?? = 0x00100000, // Spell-Editor-GUI, "Only Visible to Caster" ?? weird mix of spells
        // ?? = 0x00200000, // Spell-Editor-GUI, "Client UI Target Effects" ?? has Blizzard/Death and Decay which are targetable but also a pile of random spells
        // ?? = 0x01000000, // Spell-Editor-GUI, "Can Target Untargetable" ?? not sure how to verify
        // ?? = 0x02000000, // Spell-Editor-GUI, "Exorcism, Flash of light" ?? weird mix of spells again
        // ?? = 0x10000000, // Spell-Editor-GUI, "Death Grip" ?? many more spells than just Death Grip
        // ?? = 0x20000000, // Spell-Editor-GUI, "Not Done Percent Damage Mods" ??
        // ?? = 0x80000000, // Spell-Editor-GUI, "Ignore Category Cooldown Mods" ??
    }

    [Flags]
    public enum SpellFlags8 : uint
    {
        ReactivateOnRessurrect = 0x00000004,
        Cheat = 0x00000008,
        Totem = 0x00000020,
        NoPushbackOnDamage = 0x00000040,
        HordeOnly = 0x00000100,
        AllianceOnly = 0x00000200,
        // ?? = 0x00000400, // Spell-Editor-GUI, "Dispel Charges" ??
        OnlyInterruptsNonPlayer = 0x00000800,
        // ?? = 0x00004000, // Spell-Editor-GUI, "Raise Dead" ?? doesn't exist on any spells now
        RestoresSecondaryPower = 0x00010000,
        ChargeToTarget = 0x00040000,
        Teleport = 0x00080000,
        // ?? = 0x00100000, // Spell-Editor-GUI, "Blink, Divine Shield, Ice Block" ?? weird mix of spells
    }

    [Flags]
    public enum SpellFlags9 : uint
    {
        IsMastery = 0x20000000,
    }

    [Flags]
    public enum SpellFlags10 : uint
    {
        RestrictedFlightArea = 0x00000004,
        SummonTotem = 0x00000020,
    }

    [Flags]
    public enum SpellFlags11 : uint
    {
        NoAccountBound = 0x20000000,
    }

    [Flags]
    public enum SpellFlags12 : uint
    {

    }

    [Flags]
    public enum SpellFlags13 : uint
    {

    }

    [Flags]
    public enum SpellFlags14 : uint
    {

    }
}
