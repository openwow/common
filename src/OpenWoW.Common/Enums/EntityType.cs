﻿namespace OpenWoW.Common
{
    public enum EntityType : byte
    {
        Achievement = 1,
        Item = 2,
        Spell = 3,
    }
}
