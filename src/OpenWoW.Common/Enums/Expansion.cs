﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenWoW.Common
{
    public enum Expansion : byte
    {
        [MaxLevel(60)]
        Vanilla = 0,

        [Display(Name = "The Burning Crusade")]
        [MaxLevel(70)]
        TheBurningCrusade = 1,

        [Display(Name = "Wrath of the Lich King")]
        [MaxLevel(80)]
        WrathOfTheLichKing = 2,

        [MaxLevel(85)]
        Cataclysm = 3,

        [Display(Name = "Mists of Pandaria")]
        [MaxLevel(90)]
        MistsOfPandaria = 4,

        [Display(Name = "Warlords of Draenor")]
        [MaxLevel(100)]
        WarlordsOfDraenor = 5,

        [MaxLevel(110)]
        Legion = 6,

        [Display(Name = "Battle for Azeroth")]
        [MaxLevel(120)]
        BattleForAzeroth = 7,
    }
}
