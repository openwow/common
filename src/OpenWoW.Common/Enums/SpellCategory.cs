﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenWoW.Common
{
    public enum SpellCategory
    {
        Uncategorized = 0,

        [Display(Name = "Artifact Traits")]
        ArtifactTraits,
        [Display(Name = "Azerite Traits")]
        AzeriteTraits,
        [Display(Name = "Class Abilities")]
        ClassAbilities,
        [Display(Name = "Companions")]
        Companions,
        [Display(Name = "Glyphs")]
        Glyphs,
        [Display(Name = "Guild Perks")]
        GuildPerks,
        [Display(Name = "Mounts")]
        Mounts,
        [Display(Name = "NPC Abilities")]
        NpcAbilities,
        [Display(Name = "Perks")]
        Perks,
        [Display(Name = "Pet Abilities")]
        PetAbilities,
        [Display(Name = "Pet Talents")]
        PetTalents,
        [Display(Name = "Professions")]
        Professions,
        [Display(Name = "PvP Talents")]
        PvpTalents,
        [Display(Name = "Racial Abilities")]
        Racials,
    }
}
