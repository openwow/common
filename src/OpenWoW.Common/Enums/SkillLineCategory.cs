﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common.Enums
{
    public enum SkillLineCategory
    {
        Attributes = 5, // weird selection: Pet - Monkey, All - Guild Perks, Wrathguard, Observer, Shivarra, Voidlord, Racial - Pandaren
        Weapon = 6,
        Class = 7,
        Armor = 8,
        SecondaryProfession = 9,
        Language = 10,
        PrimaryProfession = 11,
        Generic = 12,
    }
}
