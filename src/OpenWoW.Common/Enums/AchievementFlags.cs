﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    [Flags]
    public enum AchievementFlags
    {
        Statistic = 0x00000001,
        Hidden = 0x00000002,
        NoVisual = 0x00000004,
        Cumulative = 0x00000008,
        DisplayHighest = 0x00000010,
        CriteriaCount = 0x00000020,
        AveragePerDay = 0x00000040,
        HasProgressBar = 0x00000080,
        ServerFirst = 0x00000100,
        BroadcastGuildName = 0x00000200,
        HideNameInTie = 0x00000400,
        HideUntilEarned = 0x00000800,
        AccountWide = 0x00020000,
    }
}
