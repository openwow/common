﻿namespace OpenWoW.Common
{
    public enum WowBranch : byte
    {
        None = 0,

        [GameString("wow")]
        [BuildMatch("WOW-[build]patch[patch]_Retail-")]
        Live = 1,

        [GameString("wowt")]
        [BuildMatch("WOW-[build]patch[patch]_PTR-")]
        PTR = 2,

        [GameString("wow_beta")]
        [BuildMatch("WOW-[build]patch[patch]_Beta-")]
        Beta = 3,

        [GameString("wowz")]
        [BuildMatch("WOW-[build]patch[patch]_Submission-")]
        Vendor = 4,

        // TODO: verify attributes
        [GameString("wow_classic")]
        [BuildMatch("WOW-[build]patch[patch]_Classic-")]
        Classic = 11,

        // Looks like normal beta but 1.x.y, ugh
        [GameString("wow_classic_beta")]
        [BuildMatch("WOW-[build]patch[patch]_Beta-")]
        ClassicBeta = 12,
    }

    public class WowBranchMatch
    {
        public WowBranch Branch;
        public string Patch;
        public int Build;

        public int MajorVersion;
        public int MinorVersion;
        public int PatchVersion;

        public WowBranchMatch(WowBranch branch, string patch, int build)
        {
            Branch = branch;
            Patch = patch;
            Build = build;

            var parts = Patch.Split('.');
            MajorVersion = int.Parse(parts[0]);
            MinorVersion = int.Parse(parts[1]);
            PatchVersion = int.Parse(parts[2]);
        }
    }
}
