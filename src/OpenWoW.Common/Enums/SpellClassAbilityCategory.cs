﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public enum SpellClassAbilityCategory
    {
        Baseline = 1,
        Specialization = 2,
        Talent = 3,
        Perk = 4,
        PvpTalent = 5,
    }
}
