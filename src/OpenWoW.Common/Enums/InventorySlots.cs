﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenWoW.Common
{
    public enum InventorySlot
    {
        NotEquippable = 0,

        [ItemBudgetIndex(0)]
        Head = 1,

        [ItemBudgetIndex(2)]
        Neck = 2,

        [ItemBudgetIndex(1)]
        Shoulders = 3,

        Shirt = 4,

        [ItemBudgetIndex(0)]
        Chest = 5,

        [ItemBudgetIndex(1)]
        Waist = 6,

        [ItemBudgetIndex(0)]
        Legs = 7,

        [ItemBudgetIndex(1)]
        Feet = 8,

        [ItemBudgetIndex(2)]
        Wrists = 9,

        [ItemBudgetIndex(1)]
        Hands = 10,

        [ItemBudgetIndex(2)]
        Finger = 11,

        [ItemBudgetIndex(1)]
        Trinket = 12,

        [Display(Name = "One Hand")]
        [ItemBudgetIndex(3)]
        OneHandWeapon = 13,

        [Display(Name = "Off Hand")]
        [ItemBudgetIndex(2)]
        OffHandWeapon = 14,

        [ItemBudgetIndex(0)]
        Ranged = 15,

        [ItemBudgetIndex(2)]
        Back = 16,

        [Display(Name = "Two Hand")]
        [ItemBudgetIndex(0)]
        TwoHandWeapon = 17,

        Bag = 18,

        Tabard = 19,

        [Display(Name = "Chest")]
        [ItemBudgetIndex(0)]
        Robe = 20,

        [Display(Name = "Main Hand")]
        [ItemBudgetIndex(3)]
        MHWeapon = 21,

        [Display(Name = "Off Hand")]
        [ItemBudgetIndex(3)]
        OHWeapon = 22,

        [Display(Name = "Held In Off-hand")]
        [ItemBudgetIndex(2)]
        Holdable = 23,

        Ammo = 24,

        [Display(Name = "Ranged")]
        [ItemBudgetIndex(4)]
        Thrown = 25,

        [Display(Name = "Ranged")]
        [ItemBudgetIndex(3)]
        Ranged2 = 26,

        Quiver = 27,

        [Display(Name = "Ranged")]
        Relic = 28
    }
}
