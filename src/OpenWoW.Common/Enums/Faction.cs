﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public enum Faction
    {
        Both = -1,
        Horde = 0,
        Alliance = 1,
    }
}
