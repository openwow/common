﻿using System.ComponentModel.DataAnnotations;

namespace OpenWoW.Common
{
    public enum SpellAuraModifierMisc1
    {
        [Display(Name = "Effect #1 Value")]
        Effect1Value = 3,

        [Display(Name = "Effect #2 Value")]
        Effect2Value = 12,

        [Display(Name = "Effect #3 Value")]
        Effect3Value = 23,
    }
}
