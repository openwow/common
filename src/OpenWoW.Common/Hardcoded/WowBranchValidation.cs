﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static partial class HardcodedData
    {
        public static Dictionary<WowBranch, Func<WowBranchMatch, bool>> WowBranchValidation = new Dictionary<WowBranch, Func<WowBranchMatch, bool>>
        {
            { WowBranch.Live, (wbm) => wbm.MajorVersion >= 8 },
            { WowBranch.PTR, (wbm) => wbm.MajorVersion >= 8 },
            { WowBranch.Beta, (wbm) => wbm.MajorVersion >= 8 },
            { WowBranch.Vendor, (wbm) => wbm.MajorVersion >= 8 },

            { WowBranch.Classic, (wbm) => wbm.MajorVersion == 1 },
            { WowBranch.ClassicBeta, (wbm) => wbm.MajorVersion == 1 },
        };
    }
}
