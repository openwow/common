﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static partial class HardcodedData
    {
        /// <summary>
        /// Spell IDs for the hidden specialization auras that are used for spec tuning
        /// </summary>
        public static readonly HashSet<int> SpecializationAuraSpellIDs = new HashSet<int>()
        {
            137005, // Death Knight
            137006, // Frost Death Knight
            137007, // Unholy Death Knight
            137008, // Blood Death Knight
            137009, // Druid
            137010, // Guardian Druid
            137011, // Feral Druid
            137012, // Restoration Druid
            137013, // Balance Druid
            137014, // Hunter
            137015, // Beast Mastery Hunter
            137016, // Marksmanship Hunter
            137017, // Survival Hunter
            137018, // Mage
            137019, // Fire Mage
            137020, // Frost Mage
            137021, // Arcane Mage
            137022, // Monk
            137023, // Brewmaster Monk
            137024, // Mistweaver Monk
            137025, // Windwalker Monk
            137026, // Paladin
            137027, // Retribution Paladin
            137028, // Protection Paladin
            137029, // Holy Paladin
            137030, // Priest
            137031, // Holy Priest
            137032, // Discipline Priest
            137033, // Shadow Priest
            137034, // Rogue
            137035, // Subtlety Rogue
            137036, // Outlaw Rogue
            137037, // Assassination Rogue
            137038, // Shaman
            137039, // Restoration Shaman
            137040, // Elemental Shaman
            137041, // Enhancement Shaman
            137042, // Warlock
            137043, // Affliction Warlock
            137044, // Demonology Warlock
            // 137045 is an unrelated spell
            137046, // Destruction Warlock
            137047, // Warrior
            137048, // Protection Warrior
            137049, // Arms Warrior
            137050, // Fury Warrior
            212611, // Demon Hunter
            212612, // Havoc Demon Hunter
            212613, // Vengeance Demon Hunter
        };
    }
}
