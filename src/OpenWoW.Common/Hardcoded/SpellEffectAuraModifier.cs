﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static partial class HardcodedData
    {
        public static Dictionary<int, string> SpellEffectAuraModifier = new Dictionary<int, string>
        {
            {0, "Damage/Healing" },
            {1, "Buff Duration" },
            {2, "Threat Done" },
            {3, "Effect #1 Value" },
            {4, "Buff Charges" },
            {5, "Range" },
            {6, "Radius" },
            {7, "Critical Chance" },
            {8, "Spell Effectiveness" },
            {9, "Pushback Reduction" },
            {10, "Casting Time" },
            {11, "Cooldown" },
            {12, "Effect #2 Value" },
            {14, "Power Cost" },
            {15, "Critical Damage Bonus" },
            {16, "Hit Chance" },
            {17, "Jump Targets" },
            {18, "Proc Chance" },
            {19, "Time Between Ticks" },
            {20, "Spell Damage After 1st Target" },
            {21, "Global Cooldown" },
            {22, "Periodic Damage/Healing" },
            {23, "Effect #3 Value" },
            {28, "Aura Dispel Resist Chance" },
            {32, "Chance of Being Critically Hit" },
        };
    }
}
