﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static partial class HardcodedData
    {
        public static HashSet<int> SpellPowerSpecializations = new HashSet<int>
        {
            0,  // none provided
            62, // Arcane Mage
            63, // Fire Mage
            64, // Frost Mage
            65, // Holy Paladin
            102,// Balance Druid
            105,// Restoration Druid
            256,// Discipline Priest
            257,// Holy Priest
            258,// Shadow Priest
            262,// Elemental Shaman
            264,// Restoration Shaman
            265,// Affliction Warlock
            266,// Demonology Warlock
            267,// Destruction Warlock
            270,// Mistweaver Monk
        };
    }
}
