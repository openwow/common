﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace OpenWoW.Common
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class BuildMatchAttribute : Attribute
    {
        public string BuildPrefix { get; }
        public Func<string, string, bool> ValidationFunc { get; set; } = null;

        private Regex BuildRegex { get; }

        public BuildMatchAttribute(string prefix)
        {
            BuildPrefix = prefix;

            string regex = prefix.Replace("[build]", @"(?<build>\d+)")
                .Replace("[patch]", @"(?<patch>[\d\.]+)");
            BuildRegex = new Regex(regex, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        }
        
        public WowBranchMatch RegexMatch(WowBranch branch, string buildString)
        {
            var m = BuildRegex.Match(buildString);
            if (m.Success)
            {
                var wbm = new WowBranchMatch(branch, m.Groups["patch"].ToString(), int.Parse(m.Groups["build"].ToString()));
                if (!HardcodedData.WowBranchValidation.TryGetValue(branch, out Func<WowBranchMatch, bool> validationFunc) || validationFunc(wbm))
                {
                    return wbm;
                }
            }

            return null;
        }
    }
}
