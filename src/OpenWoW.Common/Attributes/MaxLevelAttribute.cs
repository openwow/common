﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public class MaxLevelAttribute : Attribute
    {
        public int MaxLevel { get; private set; }

        public MaxLevelAttribute(int maxLevel)
        {
            MaxLevel = maxLevel;
        }
    }
}
