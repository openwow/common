﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public class ItemBudgetIndexAttribute : Attribute
    {
        public int ItemBudgetIndex { get; private set; }

        public ItemBudgetIndexAttribute(int itemBudgetIndex)
        {
            ItemBudgetIndex = itemBudgetIndex;
        }
    }
}
