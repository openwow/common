﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    [AttributeUsage(AttributeTargets.Field)]
    public class GameStringAttribute : Attribute
    {
        public string GameString { get; private set; }

        public GameStringAttribute(string gameString)
        {
            GameString = gameString;
        }
    }
}
