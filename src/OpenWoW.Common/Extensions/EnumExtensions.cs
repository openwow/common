﻿using EnumsNET;
using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenWoW.Common.Extensions
{
    public static class EnumExtensions
    {
        public static T GetAttribute<T>(this Enum value) where T : Attribute
        {
            return GetAttributes<T>(value).FirstOrDefault();
        }

        public static IEnumerable<T> GetAttributes<T>(this Enum value) where T : Attribute
        {
            return value
                .GetType()
                .GetMember(value.ToString())[0]
                .GetCustomAttributes(typeof(T), false)
                .Cast<T>();
        }

        public static string GetGameString(this WowBranch branch)
        {
            return branch.GetAttribute<GameStringAttribute>().GameString;
        }

        public static string GetName(this WowBranch branch)
        {
            return branch.AsString(EnumFormat.DisplayName, EnumFormat.Name);
        }
    }
}
