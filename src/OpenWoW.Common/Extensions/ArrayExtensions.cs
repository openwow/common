﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static class ArrayExtensions
    {
        public static T SafeGet<T>(this T[] array, int index)
        {
            if (array != null && index < array.Length)
            {
                return array[index];
            }
            else
            {
                return default(T);
            }
        }

        public static string ToHexString(this byte[] array)
        {
            StringBuilder sb = new StringBuilder(array.Length * 2);
            foreach (byte b in array)
            {
                sb.AppendFormat("{0:x2}", b);
            }
            return sb.ToString();
        }
    }
}
