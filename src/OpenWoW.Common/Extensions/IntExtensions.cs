﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static class IntExtensions
    {
        public static int CountBits(this int value)
        {
            value = value - ((value >> 1) & 0x55555555);
            value = (value & 0x33333333) + ((value >> 2) & 0x33333333);
            return (((value + (value >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
        }
    }
}
