﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static class DoubleExtensions
    {
        // Leagcy - I don't know why we use this
        public static double SuperRound(this double num)
        {
            if (num >= 1 || num <= -1)
                return Math.Round(num);

            return Math.Round(num, 1);
        }
    }
}
