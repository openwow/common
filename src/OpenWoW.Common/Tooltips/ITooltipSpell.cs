﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public interface ITooltipSpell
    {
        int SpellID { get; }
        string Name { get; }
        string ParsedDescription { get; }

        string CastTimeText { get; }
        string CooldownText { get; }
        string GlobalCooldownText { get; }
        string PowerText { get; }
        string ProcsPerMinuteText { get; }
        string RangeText { get; }
        string RankText { get; }
    }
}
