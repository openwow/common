﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OpenWoW.Common.Utilities
{
    public class StopwatchTimer : IDisposable
    {
        private readonly string _format;
        private readonly NLog.Logger _logger;
        private readonly Stopwatch _stopwatch;

        public StopwatchTimer(NLog.Logger logger, string format)
        {
            _logger = logger;
            _format = format;
            _stopwatch = Stopwatch.StartNew();
        }

        public void Dispose()
        {
            _stopwatch.Stop();
            _logger.Debug(_format, _stopwatch.Elapsed);
        }
    }
}
