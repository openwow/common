﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OpenWoW.Common
{
    public static class TimeUtilities
    {
        public static void Time(Action action, Action<TimeSpan> logAction)
        {
            var sw = Stopwatch.StartNew();
            action();
            sw.Stop();
            logAction(sw.Elapsed);
        }

        public static T1 Time<T1>(Func<T1> func, Action<TimeSpan> logAction)
        {
            var sw = Stopwatch.StartNew();
            var ret = func();
            sw.Stop();
            logAction(sw.Elapsed);
            return ret;
        }

        private const int MILLISECONDS_IN_MINUTE = 60 * 1000;
        private const int MILLISECONDS_IN_HOUR = 60 * MILLISECONDS_IN_MINUTE;
        private const int MILLISECONDS_IN_DAY = 24 * MILLISECONDS_IN_HOUR;
        /// <summary>
        /// Converts ms to text for the duration of the spell in the tooltip.
        /// </summary>
        public static string GetTooltipTime(int ms, string retThisIfInvalid, bool longNames = false)
        {
            if (ms == 0 || ms == -1)
            {
                return retThisIfInvalid;
            }

            double result = Math.Abs(ms);
            string name;

            if (result > MILLISECONDS_IN_DAY)
            {
                result /= MILLISECONDS_IN_DAY;
                name = result == 1 ? "day" : "days";
            }
            else if (result > MILLISECONDS_IN_HOUR)
            {
                result /= MILLISECONDS_IN_HOUR;
                name = longNames ? (result == 1 ? "hour" : "hours") : "hr";
            }
            else if (result > MILLISECONDS_IN_MINUTE)
            {
                result /= MILLISECONDS_IN_MINUTE;
                name = longNames ? (result == 1 ? "minute" : "minutes") : "min";
            }
            else
            {
                result /= 1000;
                name = longNames ? (result == 1 ? "second" : "seconds") : "sec";
            }

            return $"{ Math.Round(result, 1) } { name }";
        }
    }
}
