﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Common
{
    public static class JsonUtilities
    {
        private static readonly Dictionary<Type, JsonSerializerSettings> _settingsCache = new Dictionary<Type, JsonSerializerSettings>();

        public static string SerializeInterface<TInterface>(TInterface thing) where TInterface : class
        {
            var interfaceType = typeof(TInterface);
            if (!_settingsCache.TryGetValue(interfaceType, out JsonSerializerSettings settings))
            {
                settings = _settingsCache[interfaceType] = new JsonSerializerSettings()
                {
                    ContractResolver = new InterfaceContractResolver<TInterface>(),
                };
            }
            return JsonConvert.SerializeObject(thing, settings);
        }
    }

    public class InterfaceContractResolver<TInterface> : DefaultContractResolver where TInterface : class
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return base.CreateProperties(typeof(TInterface), memberSerialization);
        }
    }
}
