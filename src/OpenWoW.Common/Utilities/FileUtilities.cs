﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OpenWoW.Common
{
    public static class FileUtilities
    {
        private static readonly string _directorySeparator = Path.DirectorySeparatorChar.ToString();
        private static readonly Dictionary<string, string> _pathCache = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);

        public static string GetFilenameCaseInsensitive(params string[] pathParts)
        {
            if (pathParts.Length > 1)
            {
                //var sigh = new List<string> { pathParts[0] };
                string testPath = Path.Combine(pathParts.Take(pathParts.Length - 1).ToArray());
                if (!_pathCache.TryGetValue(testPath, out string finalPath))
                {
                    var actualPath = new List<string> { Path.GetFullPath(pathParts[0]) };
                    var sigh = pathParts.Skip(1).Take(Math.Max(0, pathParts.Length - 2)).SelectMany(x => x.Split(Path.DirectorySeparatorChar)).ToList();

                    var currentDirectory = new DirectoryInfo(actualPath[0]);
                    foreach (string directory in sigh)
                    {
                        bool found = false;
                        foreach (var di in currentDirectory.GetDirectories())
                        {
                            if (di.Name.Equals(directory, StringComparison.InvariantCultureIgnoreCase))
                            {
                                actualPath.Add(di.Name);
                                currentDirectory = new DirectoryInfo(Path.Combine(actualPath.ToArray()));
                                found = true;
                                break;
                            }
                        }

                        if (!found)
                        {
                            throw new DirectoryNotFoundException($"Unable to find '{directory}' in '{ string.Join(_directorySeparator, actualPath) }'");
                        }
                    }

                    finalPath = string.Join(_directorySeparator, actualPath);
                    _pathCache[finalPath] = finalPath;
                }

                var finalDirectory = new DirectoryInfo(finalPath);
                string findFilename = pathParts.Last();
                foreach (FileInfo fi in finalDirectory.GetFiles())
                {
                    if (fi.Name.Equals(findFilename, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return Path.Combine(finalPath, fi.Name);
                    }
                }

                foreach (DirectoryInfo di in finalDirectory.GetDirectories())
                {
                    if (di.Name.Equals(findFilename, StringComparison.InvariantCultureIgnoreCase))
                    {
                        return Path.Combine(finalPath, di.Name);
                    }
                }

                throw new FileNotFoundException($"Unable to find '{findFilename}' in '{ finalPath }'");
            }

            return null;
        }

        private static readonly Regex _annoyingCharacterRegex = new Regex(@"[\.\- ]", RegexOptions.Compiled);
        private static readonly Regex _underscoreRegex = new Regex("[_]{2,}", RegexOptions.Compiled);
        private static readonly Regex _extensionRegex = new Regex("_(blp|tga)", RegexOptions.Compiled);
        public static string SanitizeFilename(string filename)
        {
            if (filename == null)
            {
                throw new ArgumentNullException(nameof(filename));
            }

            // Remove path information
            int index = filename.LastIndexOf("\\");
            if (index >= 0)
            {
                filename = filename.Substring(index + 1);
            }
            index = filename.LastIndexOf("/");
            if (index >= 0)
            {
                filename = filename.Substring(index + 1);
            }

            filename = filename.ToLowerInvariant();

            filename = _annoyingCharacterRegex.Replace(filename, "_");
            filename = _underscoreRegex.Replace(filename, "_");
            filename = _extensionRegex.Replace(filename, "");

            filename = filename.TrimEnd('_');

            return filename;
        }
    }
}
